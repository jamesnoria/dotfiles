# My DotFiles and Automations

-   This repository has all my "dot-files" that I use as a GNU-Linux user, also I decided to include some Bash scripts that allows me to make my life easier as a software developer.
    
-   Feel free to use it as better you consider.
## Contact me:
- [![ProtonMail Badge](https://img.shields.io/badge/jamesnoria@pm.me-8B89CC?style=for-the-badge&logo=protonmail&logoColor=white&link=mailto:jamesnoria@pm.me)](mailto:jamesnoria@pm.me) [![Linkedin Badge](https://img.shields.io/badge/jamesnoria-0077B5?style=for-the-badge&logo=linkedin&logoColor=white&link=https://www.linkedin.com/in/jamesnoria)](https://www.linkedin.com/in/jamesnoria) [![Twitter Badge](https://img.shields.io/badge/@jamesnoria-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white&link=https://twitter.com/jamesnoria)](https://twitter.com/jamesnoria)